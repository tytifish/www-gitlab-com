---
layout: handbook-page-toc
title: "GitLab System Admin Basics Hands On Guide- Lab 2"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab System Admin Basics course."
---
# GitLab System Admin Basics Hands On Guide- Lab 2
{:.no_toc}

## LAB 2- VERIFY YOUR GITLAB INSTANCE  

### On your local desktop, open your terminal/command prompt

1. With your cursor in the terminal field, type: gitlab-ctl status and press **Enter**.  
2. Review the results to verify the services for your instance are running properly. 
3. Run through the list of services running and verify you have what you need for your GitLab install.

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab System Admin Basics- please submit your changes via Merge Request!

