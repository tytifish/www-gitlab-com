---
layout: job_family_page
title: "Renewals Operations Analyst"
description: "The Renewal Analyst Team is responsible for providing forecast clarity, scoring renewal growth and retention likelihood, and developing retention action plans."
---

The Renewal Analyst Team is responsible for providing forecast clarity, scoring renewal growth and retention likelihood, and developing retention action plans. Key objectives include:

* Optimize and scale the renewal management and bookings forecast process for predictability
* Develop and refine renewal health to score Accounts with a high likelihood of growth 
* Drive renewal best practices portfolio-wide, assisting the build out of training for field team
* Extract insights from renewal data and recommend tactics to improve results
* Collaborate with the Customer Success Team to identify and help build out vital collateral for retention

## Levels 

### Renewals Operations Analyst (Intermediate)

The Renewals Operations Analyst (Intermediate) reports to the [Senior Manager Customer Success Operations](/job-families/sales/sales-operations/#senior-manager-sales-operations).

#### Renewals Operations Analyst (Intermediate) Job Grade

The Renewals Operations Analyst (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Renewals Operations Analyst (Intermediate) Responsibilities

* Maintain and report an accurate rolling 12+ month forecast of renewals for the global Commercial Sales Team
* Build and maintain reports for key metrics such as renewal revenue, bookings, retention rate, and overall customer health
* Actively engage with key sales leaders and decision-makers to identify customer requirements and uncover roadblocks to ensure on-time renewals and retention goals
* Work with cross-functional teams like Product, Strategy, and Finance to champion impactful process improvement and automation for the benefit of Renewals
* Develop playbooks for renewal engagement maximizing revenue retention
* Identify opportunities for process automation and optimization, with a focus on scalability and driving significant growth
* Improve the sales team’s effectiveness and efficiency and provide increased customer insights through data, automation, and product analytics
* Proactively drive a high degree of CRM data quality by leveraging creative system solutions and delivering end-user training

#### Renewals Operations Analyst (Intermediate) Requirements

* 3+ years of Operations experience supporting Sales / Customer Success / Account Management teams, preferably within an Enterprise SaaS organization
* 2+ years of Sales or Customer Success experience
* Proven track-record of increasing renewal rates
* Experience implementing renewal processes and metrics
* Able to teach best practices, strategies, and tactics
* Strong analytical ability and able to prioritize multiple projects
* Salesforce experience and knowledge of enterprise SaaS tools
* Excellent problem solving, project management, interpersonal and organizational skills
* Experience creating a framework for a newly created team, as well as with expanding the team when applicable
* SaaS and B2B experience preferred
* Experience with Customer Success Management systems (e.g., Gainsight/Totango/Client Success/etc., digital marketing tools) preferred
* Interest in GitLab, and open-source software
* You share our values and work by those values
* Ability to use GitLab
* Experience with agile/DevOps and/or SDLC process and/or tools is a plus

### Senior Renewals Operations Analyst

The  Senior Renewals Operations Analyst reports to the [Senior Manager Customer Success Operations](/job-families/sales/sales-operations/#senior-manager-sales-operations).

####  Senior Renewals Operations Analyst Job Grade

The  Senior Renewals Operations Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

####  Senior Renewals Operations Analyst Responsibilities

* Extends that of the Renewals Operations Analyst (Intermediate) responsibilities
* Own, drive, and lead the renewals process in collaboration with the sales team to ensure we hit company objectives

#### Senior Renewals Operations Analyst Requirements

* Extends that of the Renewals Operations Analyst (Intermediate) requirements
* 5+ years of Operations experience supporting Sales / Customer Success / Account Management teams, preferably within an Enterprise SaaS organization
* 3+ years of Sales or Customer Success experience


## Performance Indicators

* [SMAU](/handbook/sales/performance-indicators/#active-smau-for-paying-customers)
* [CSAT](/handbook/business-ops/metrics/#customer-satisfaction-survey-csat)
* [PNPS](/handbook/product/performance-indicators/#gitlab-com-paid-net-promoter-score-pnps)
* [Churn & customer retention metrics](/handbook/customer-success/vision/#retention-and-reasons-for-churn)

## Career Ladder

The next step in the Renewals Operations job family is not yet defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 Team Members

Additional details about our process can be found on our [hiring page](/handbook/hiring).
